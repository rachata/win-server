var app = require('express')();
var mysql = require('mysql');
var crypto = require('crypto');
var request = require('request')
var dateFormat = require('dateformat');
var bodyParser = require('body-parser');
const { exit } = require('process');

var port = process.env.PORT || 8989;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var con = mysql.createConnection({
  host: "35.240.153.210",
  user: "win",
  password: "@#WinDB2020",
  database: "win"
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});


con.connect(function (err) {
  if (err) throw err;
});




app.post('/machine', function (req, res) {

  var name = req.body.name;
  var lat = req.body.lat;
  var lng = req.body.lng;

  var area = req.body.area;


  console.log(JSON.stringify(req.body));
  var query = "insert into machine (name , lat , lng) values ('" + name + "' , " + lat + " , " + lng + ")";
  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    var idMachine = result.insertId

    if (Object.keys(area).length >= 1) {

      for (var i = 0; i < Object.keys(area).length; i++) {
        var latArea = area[i]['lat'];
        var lngArea = area[i]['lng'];

        var query = "insert into area_machine (id_machine , lat , lng) values (" + idMachine + " , " + latArea + " , " + lngArea + ")";

        con.query(query, function (err, result, fields) {
          if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
          }
        });

      }
      res.json({ "status": 200 })

    } else {
      res.json({ "status": 200 })
    }
  });
});

app.post('/area', function (req, res) {

  var id = req.body.id;
  var area = req.body.area;


  if (Object.keys(area).length >= 1) {

    for (var i = 0; i < Object.keys(area).length; i++) {
      var latArea = area[i]['lat'];
      var lngArea = area[i]['lng'];

      var query = "insert into area_machine (id_machine , lat , lng) values (" + id + " , " + latArea + " , " + lngArea + ")";

      con.query(query, function (err, result, fields) {
        if (err) {
          console.log(err.stack);
          res.json({ "status": 500, message: err.stack })
          return;
        }
      });

    }
    res.json({ "status": 200 })

  } else {
    res.json({ "status": 200 })
  }
});

app.post('/area/delete', function (req, res) {

  var name = req.body.name;



  var query = "delete from machine WHERE name = '" + name + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }
    res.json({ "status": 200 })
  });
});

app.get('/machine', function (req, res) {

  var query = "select name , machine.lat as mlat , machine.lng as mlng , status , last_connect , area_machine.lat , area_machine.lng  from machine left join area_machine on machine.id = area_machine.id_machine";

  var listMachine = [];
  var nameMachine = [];
  var resultMechine = {};
  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    for (var i = 0; i < Object.keys(result).length; i++) {


      var temp = {};
      if (nameMachine.length == 0) {

        nameMachine.push(result[i]['name'])

        temp['name'] = result[i]['name']
        temp['lat'] = result[i]['mlat']
        temp['lng'] = result[i]['mlng']
        temp['status'] = result[i]['status']
        temp['lastConnect'] = result[i]['last_connect']

        var listArea = [];
        var tempArea = {};

        tempArea['lat'] = result[i]['lat']
        tempArea['lng'] = result[i]['lng']

        listArea.push(tempArea)

        temp['area'] = listArea;
        listMachine.push(temp)

      } else {

        if (!nameMachine.includes(result[i]['name'])) {

          nameMachine.push(result[i]['name'])

          var temp = {};

          temp['name'] = result[i]['name']
          temp['lat'] = result[i]['mlat']
          temp['lng'] = result[i]['mlng']
          temp['status'] = result[i]['status']
          temp['lastConnect'] = result[i]['last_connect']

          var listArea = [];
          var tempArea = {};

          tempArea['lat'] = result[i]['lat']
          tempArea['lng'] = result[i]['lng']

          listArea.push(tempArea)

          temp['area'] = listArea;

          listMachine.push(temp)
        } else {

          var tempArea = {};

          tempArea['lat'] = result[i]['lat']
          tempArea['lng'] = result[i]['lng']

          for (var j = 0; j < Object.keys(listMachine).length; j++) {

            if (listMachine[j]['name'] == result[i]['name']) {

              listMachine[j]['area'].push(tempArea);
            }
          }
        }

      }
    }



    resultMechine['data'] = listMachine
    res.json({ "status": 200, message: resultMechine })
  });


})


app.get('/uuid', function (req, res) {

  var query = "SELECT UUID() as uid";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200, "uid": result[0]['uid'] })

  });
})

app.post('/uuid', function (req, res) {

  var uuid = req.body.uuid;
  var note = req.body.note;


  var query = "select * from uid  WHERE text = '" + uuid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {
      res.json({ "status": 204 })
    } else {

      var query = "insert into uid (text , note) values ('" + uuid + "' , '" + note + "')";
      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }


        res.json({ "status": 200 })
      });
    }


  });

})


app.get('/uid', function (req, res) {

  var query = "select * from uid";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200, "message": result })

  });
})


app.post('/uuid/delete', function (req, res) {

  var text = req.body.text;



  var query = "delete from uid WHERE text = '" + text + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }
    res.json({ "status": 200 })
  });
});

app.post('/log', function (req, res) {



  var obj = JSON.parse(JSON.stringify(JSON.parse(req.body.log)))


  console.log("/log " + JSON.stringify(obj));
  if (!obj.hasOwnProperty("uid")) {
    res.json({ "status": 204, "message": "Not successful, no uid" })

    return;
  }

  if (!obj.hasOwnProperty("pm25")) {
    res.json({ "status": 204, "message": "Not successful, no pm25" })
    return;
  }


  if (!obj.hasOwnProperty("pm10")) {
    res.json({ "status": 204, "message": "Not successful, no pm10" })
    return;
  }


  if (!obj.hasOwnProperty("so2")) {
    res.json({ "status": 204, "message": "Not successful, no so2" })
    return;
  }


  if (!obj.hasOwnProperty("o3")) {
    res.json({ "status": 204, "message": "Not successful, no o3" })
    return;
  }


  if (!obj.hasOwnProperty("co")) {
    res.json({ "status": 204, "message": "Not successful, no co" })
    return;
  }

  if (!obj.hasOwnProperty("no2")) {
    res.json({ "status": 204, "message": "Not successful, no no2" })
    return;
  }

  var uid = obj.uid;
  var pm25 = obj.pm25;
  var pm10 = obj.pm10;
  var so2 = obj.so2;
  var o3 = obj.o3;
  var co = obj.co;
  var no2 = obj.no2;

  var query = "select * from machine  WHERE uid = '" + uid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]['id'];
      var query = "insert into log_machine (id_machine , pm25 , pm10 ,so2 , o3 , co , no2) values (" + id + " , " + pm25 + " , " + pm10 + " ," + so2 + " , " + o3 + ", " + co + " ," + no2 + ")"

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }
        res.json({ "status": 200, message: "success" });
      })
    } else {
      res.json({ "status": 204, "message": "The UID has not been paired yet." })
    }
  });

});


app.post('/v2/log', function (req, res) {


  console.log("/v2/log " + JSON.stringify(req.body));
  if (!req.body.hasOwnProperty("uid")) {
    res.json({ "status": 204, "message": "Not successful, no uid" })

    return;
  }

  if (!req.body.hasOwnProperty("pm25")) {
    res.json({ "status": 204, "message": "Not successful, no pm25" })
    return;
  }


  if (!req.body.hasOwnProperty("pm10")) {
    res.json({ "status": 204, "message": "Not successful, no pm10" })
    return;
  }


  if (!req.body.hasOwnProperty("so2")) {
    res.json({ "status": 204, "message": "Not successful, no so2" })
    return;
  }


  if (!req.body.hasOwnProperty("o3")) {
    res.json({ "status": 204, "message": "Not successful, no o3" })
    return;
  }


  if (!req.body.hasOwnProperty("co")) {
    res.json({ "status": 204, "message": "Not successful, no co" })
    return;
  }

  if (!req.body.hasOwnProperty("no2")) {
    res.json({ "status": 204, "message": "Not successful, no no2" })
    return;
  }

  var uid = req.body.uid;
  var pm25 = req.body.pm25;
  var pm10 = req.body.pm10;
  var so2 = req.body.so2;
  var o3 = req.body.o3;
  var co = req.body.co;
  var no2 = req.body.no2;

  var query = "select * from machine  WHERE uid = '" + uid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]['id'];
      var query = "insert into log_machine (id_machine , pm25 , pm10 ,so2 , o3 , co , no2) values (" + id + " , " + pm25 + " , " + pm10 + " ," + so2 + " , " + o3 + ", " + co + " ," + no2 + ")"

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }
        res.json({ "status": 200, message: "success" });
      })
    } else {
      res.json({ "status": 204, "message": "The UID has not been paired yet." })
    }
  });

});



app.listen(port, function () {
  console.log('Starting node.js on port ' + port);
});

