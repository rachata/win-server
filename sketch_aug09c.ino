#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include "coap_client.h"
#define SECONDS_DS(seconds) ((seconds)*1000000UL)

coapClient coap;

const char* ssid = "Pom_2.4G";
const char* password = "0819699010";

IPAddress ip(35,240,153,210);
int port = 3122;
char* path = "log";

StaticJsonBuffer<200> jsonBuffer;
JsonObject& root = jsonBuffer.createObject();

void callback_response(coapPacket &packet, IPAddress ip, int port) {
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;

  if (packet.type == 3 && packet.code == 0) {
    Serial.println("ping ok");
  }
  Serial.println(p);
}

void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println(" ");

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());

  coap.response(callback_response);
  coap.start();

}

void loop() {


  root["uid"] = "56a3849d-f723-11ea-bcb4-42010a940002";
  root["pm25"] = 22.3;
  root["pm10"] = 33.3;
  root["so2"] = 10;
  root["o3"] = 34;
  root["co"] = 100;
  root["no2"] = 22;

  String data;
  root.printTo(data);
  char dataChar[data.length() + 1];
  data.toCharArray(dataChar, data.length() + 1);
  bool state;

  int msgid = coap.post(ip, port, path, dataChar, data.length());
  state = coap.loop();

  delay(3000);
}
