
## ขั้นตอนการติดตั้ง CoAP Server สำหรับ Python

 - Update packet linux ให้เรียบร้อย ด้วยคำสั่ง `sudo apt-get update` และตามด้วย `sudo apt-get upgrade`
 - ติดตั้ง Python 2 ให้เรียบร้อยด้วยคำสั่ง `sudo apt install python2.7 python-pip`
 - จากนั้น รันคำสั่ง `git clone https://github.com/Tanganelli/CoAPthon.git`
 - จากนั้นรันคำสั่ง `cd CoAPthon`
 - จากนั้นรันคำสั่ง `python setup.py sdist`
**จากนั้นสามารถพัฒนาต่อได้จากไฟล์ `coapserver.py`**

 ## ขั้นตอนการติดตั้ง MQTT Server
 

 - รันคำสั่ง`sudo apt-get install mosquitto` คำสั่งนี้จะติดตั้ง Mqtt Broker
 - รันคำสั่ง `sudo apt-get install mosquitto-clients`  คำสั่งนี้จะติดตั้ง Mqtt Client เอาไว้สำหรับทดสอบการใช้งาน หากไม่ต้องการ ก็ไม่ต้องติดตั้งก็ได้
 - จากนั้นทำการเปิด Port สำหรับใช้งานคือ Port `1883` ด้วยคำสั่ง `ufw allow 1883`
 - จากนั้นลองรันคำสั่ง `netstat -plaut` จะเห็น Port 1883 ใช้งานอยู่
 - จากนั้นรันคำสั่ง `sudo mosquitto_passwd -c /etc/mosquitto/passwd win` เพื่อตั้ง Username Password `win` คือ Username
 - จากนั้นรับคำสั่ง `sudo nano /etc/mosquitto/conf.d/default.conf` เพื่อสร้างไฟล์ Config Username Password และทำการเขียน Script ลงไปในไฟล์ ดังนี้

> allow_anonymous false
> 
> password_file /etc/mosquitto/passwd

 - จากนั้นทำการ Restart Service MQTT ด้วยคำสั่ง `sudo systemctl restart mosquitto`

## ขั้นตอนการติดตั้ง NodeJS เพื่อทำเป็น HTTP Server

 - รันคำสั้ง `sudo apt install nodejs` เพื่อติดตั้ง NodeJS
 - รันคำสั่ง `sudo apt install npm`  เพื่อติดตั้ง Node Packet Manager

		
## ขั้นตอนการติดตั้ง MYSQL Server

 - รันคำสั่ง `sudo apt install mysql-server` เพื่อติดตั้ง mysql server
 - รัยคำสั่ง `sudo mysql_secure_installation` เพื่อตั้ง Password ใหม่
 - ทำการ Login Mysql ด้วยคำสั่ง `mysql -u root -p` กด Enter จากนั้นป้อน รหัสผ่านที่ตั้งไป
 - จากนั้นการทำการสร้าง Database ชื่อว่า `win` ด้วยคำสั่ง `create database win default char set utf8 collate utf8_general_ci;`
 - จากนั้นทำการสร้าง Username Password สำหรับ win database ด้วยคำสั่ง `CREATE USER 'username'@'%' IDENTIFIED BY 'password';`
 - จากนั้นกำหนดสิทธิ์การเข้าถึง win database ผ่าน Username ที่สร้างขั้นมา ด้วยคำสั่ง `GRANT ALL PRIVILEGES ON win . * TO 'username'@'%';` จากนั้นตามด้วยคำสั่ง `FLUSH PRIVILEGES;` จากนั้น `exit;`
 - จากนั้นทำการเปิด Port 3306 ด้วยคำสั่ง `ufw allow 3306`
 - จากนั้นทำการ Bind Address Mysql ให้สามารถเข้ามาใช้งานจากที่ไหนก็ได้ โดยการแก้ไขไฟล์ `sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf` แก้ `Script  bind-address  = 0.0.0.0`
 - จากนั้น Restart Service `systemctl restart mysql.service`